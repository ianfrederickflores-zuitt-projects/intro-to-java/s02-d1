package com.flores.s02;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class S02 {
    public static void main(String[] args){
        System.out.println("Arrays, Operators and Control Structures\n");
//      Arrays
//        dataType[] identifier = new dataType[size];
//        dataType identifier[] = new dataType[size];
        int[] intArray = new int[3];

//        Checking the initial values
        System.out.println("index 0: " + intArray[0]);
        System.out.println("index 1: " + intArray[1]);
        System.out.println("index 2: " + intArray[2]);
//        System.out.println(intArray[3]);

//        Manual Initialization
        intArray[0] = 100;
        intArray[1] = 200;
        intArray[2] = 300;

//        Check the new values
        System.out.println("index 0: " + intArray[0]);
        System.out.println("index 1: " + intArray[1]);
        System.out.println("index 2: " + intArray[2]);

//        Alternative way of declaring old-school array type
        int[] intArray2 = {400, 500, 600};

//        Demo of Java string interpolation
//        format specifier => %<char> => e.g. %d, %s, etc.
        System.out.println(String.format("index 0: %d ", intArray2[0]));
        System.out.println(String.format("index 1: %d ", intArray2[1]));
        System.out.println(String.format("index 2: %d ", intArray2[2]));

//        ArrayList

//        syntax => ArrayList<Data_Type> identifier = new ArrayList<Data_Type>();
        ArrayList<String> students = new ArrayList<String>();

        System.out.println(students);

//        Adding elements
        students.add("John");
        students.add("Paul");
        System.out.println(students);

//        Retrieving elements
        System.out.println(students.get(0));
        System.out.println(students.get(1));

//        Updating elements
        students.set(1, "George");
        System.out.println(students.get(1));
        System.out.println(students);

//        Deleting elements
        students.remove(1);
        System.out.println(students);

//        ArrayList with initial values
        ArrayList<String> employees = new ArrayList<String>(Arrays.asList("June", "Albert"));
        System.out.println(employees);

//        HashMaps

//        Syntax => HashMap<Key_Data_Type, Value_Data_Type> identifier = new HashMap<Key_Data_Type, Value_Data_Type>();

        HashMap<String, String> jobPositions = new HashMap<String, String>();

        System.out.println(jobPositions);

//        Creating elements
        jobPositions.put("Brandon", "Student");
        jobPositions.put("Alice", "Dreamer");

        System.out.println(jobPositions);

//        Retrieving elements
        System.out.println(jobPositions.get("Alice"));

//        Updating Elements
        jobPositions.put("Alice", "Writer");

        System.out.println(jobPositions);

//        Removing Elements
        jobPositions.remove("Brandon");

        System.out.println(jobPositions);

//        Control Statements

//        if statement
        int num1 = 10;
        int num2 = 20;

        if (num1 > 5){
            System.out.println("num1 is greater than 5");
        }

//        if-else statement
        if (num2 > 100){
            System.out.println("num2 is greater than 100");
        }

        else{
            System.out.println("num2 is less than 100");
        }

//        Logical Operators

//        OR => ||
//        AND => &&

        int juanAge = 25;
        int legalAge = 21;
        int scholarshipGrantAge = 24;

        if((juanAge >= legalAge) && (juanAge >= scholarshipGrantAge)) {
            System.out.println("Juan is of legal age and eligible for a scholarship grant.");
        }

        else{
            System.out.println("Juan is maybe not of legal age or may not be eligible for a scholarship grant.");
        }

        if((juanAge >= legalAge) || (juanAge >= scholarshipGrantAge)) { //either conditions are true
            System.out.println("Juan is either of legal age or eligible for a scholarship grant.");
        }

        else{ //both conditions false
            System.out.println("Juan is neither of legal age nor eligible for a scholarship grant.");
        }

//        Switch Statement
        int direction = 5;
        switch (direction){
            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default:
                System.out.println("Invalid input");
        }
    }
}
